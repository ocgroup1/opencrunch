from django.apps import AppConfig


class CrunchCoreConfig(AppConfig):
    name = 'crunchcore'
