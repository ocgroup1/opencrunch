# Part of OpenCrunch, see LICENSE.md and README.md
# https://gitlab.com/ocgroup1/opencrunch/
# c) 2020 Dan Ladds AGPL v3

from django.contrib import admin
from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin, PolymorphicChildModelFilter

from . import models

admin.site.register(models.Entity)
admin.site.register(models.EntityLanguage)
admin.site.register(models.PhoneNumber)
admin.site.register(models.MailMessageAddress)
admin.site.register(models.Account)
admin.site.register(models.UnitType)
admin.site.register(models.Action)
admin.site.register(models.Transaction)
