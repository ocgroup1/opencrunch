from django.shortcuts import render, get_object_or_404
from django.views import generic, View
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import views as djangoViews
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.forms.models import model_to_dict
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models as djangoModels
from django_countries.fields import Country

from . import forms
from . import models

# Framework Features
class Framework(LoginRequiredMixin, View):
    def get(self, request):
        return HttpResponse(render(request, 'ocfinance/framework.html'))

# System Pages
class Dashboard(LoginRequiredMixin, View):
    def get(self, request):
        return HttpResponse(render(request, 'ocfinance/dashboard.html'))

class Profile(LoginRequiredMixin, View):
    def get(self, request):
        return HttpResponse(render(request, 'ocfinance/profile.html'))

class Settings(LoginRequiredMixin, View):
    def get(self, request):
        return HttpResponse(render(request, 'ocfinance/settings.html'))

# Authentication
class Login(djangoViews.LoginView):
    authentication_form = forms.Login
    template_name="user/login.html"
    
class PasswordReset(djangoViews.PasswordResetView):
    form_class = forms.PasswordReset
    template_name="user/password_reset.html"
    email_template_name = "user/password_reset_mail.html"
    success_url=reverse_lazy("ocfinance:password_reset_sent")
    
class PasswordResetSent(djangoViews.PasswordResetDoneView):
    template_name="user/password_reset_sent.html"
    
class PasswordResetConfirm(djangoViews.PasswordResetConfirmView):
    form_class = forms.SetPassword
    template_name="user/password_reset_confirm.html"
    success_url=reverse_lazy("ocfinance:password_reset_complete")

class PasswordResetComplete(djangoViews.PasswordResetCompleteView):
    template_name="user/password_reset_complete.html"

class PasswordChange(djangoViews.PasswordChangeView):
    template_name="user/password_change.html"

class PasswordChangeDone(djangoViews.PasswordChangeDoneView):
    template_name="user/password_change_done.html"

# JSON Handling
# TODO: Probably put this somewhere else i.e. in core
"""JSON encoder that knows how to handle extra fields

Handles ImageFieldFile, Country"""
class JsonEncoder(DjangoJSONEncoder):
    def default(self, o):
        if isinstance(o, djangoModels.fields.files.ImageFieldFile):
            return o.name
        elif isinstance(o, Country):
            return o.name
        else:
            return super().default(o)

# Contacts
class Contacts(View):
    def get(self, request):
        return HttpResponse(render(request, "ocfinance/contacts_all.html"))
        
class AllContacts(View):
    def get(self, request):
        items = tuple(map(lambda x : model_to_dict(x), models.Entity.objects.all()))
        
    
        return JsonResponse(items, safe=False, encoder=JsonEncoder)

class StaffData(View):
    def get(self, request):
        return JsonResponse({'foo' : 'bar',})

class CustomerData(View):
    def get(self, request):
        return JsonResponse({'foo' : 'bar',})

class SupplierData(View):
    def get(self, request):
        return JsonResponse({'foo' : 'bar',})
        
class NewContact(View):
    def get(self, request):
        return JsonResponse({'foo' : 'bar',})


