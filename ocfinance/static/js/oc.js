$(function() {
    // Load dashboard
    $('#page-pane').load('/dashboard/', function(){
        return 0
    });
    
    // General page change
    $('.load-main').click(function(e) {
        e.preventDefault();
        
        $('#page-pane').load(
            $(this).attr('href'),
            function() {
                return 0
            }
        );
    });
    
    // Password reset
    $('.load-modal-reset').click(function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        
        $('#pwResetModal .modal-body').load(
            url,
            function() {
                $('#pwResetModal').modal('show');
                
                $('#pwResetModal .btn-primary').click(function(e) {
                    e.preventDefault();
                    var form = $('#pwResetModal form');
                    
                    $.post(
                        url,
                        $(form).serialize(),
                        function(response) {
                            $('#pwResetModal .modal-body').html(response);
                            
                        }
                    );
                });
            }
        );
    });
});
