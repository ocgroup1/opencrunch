# OpenCrunch

OpenCrunch is an extensible, web-based accounting system. It's completely free, open source software. It's written in Python and uses the Django framework.

Please note: this project is in its very early stages and few things are working yet.

The aim is to build a highly generalised accounting system that can be used not only for financial accounting, but also environmental accouting, asset inventory, timebanking and so forth.

# Requirements

Python 3.7
For python dependencies please see requirements.txt

JQuery 3.4.1
Popper 2.2.1
Bootstrap 4.4.1
SB Dashboard (https://github.com/BlackrockDigital/startbootstrap-sb-admin-2) 4.0.7

Python dependencies should be configured according to your OS or virtual environment
JS dependencies reside at ../ocassets relative to the git root and are symlinked to appropriate locations in ocfinance/assets/. The release build will copy these files into their appropriate location for release bundling.

If your operating system does not support symlinks please set core.symlinks to false in Git and use the text files to determine or script which files to copy. Please be careful to add these files to .gitignore and not to copy dependencies into the repository.

Incorporation of upstream releases will be by consensus.

Development stack:
Python 3.7.6
Fedora 31 GNU/Linux 5.5.13-200.fc31.x86_64
Firefox 74
Gedit

Git: https://gitlab.com/ocgroup1/opencrunch/

# License
Copyright (C) 2020 Dan Ladds

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
