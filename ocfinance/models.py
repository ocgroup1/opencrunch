# Part of OpenCrunch, see LICENSE.md and README.md
# https://gitlab.com/ocgroup1/opencrunch/
# (c) 2020 Dan Ladds AGPL v3

from django.db import models
from decimal import Decimal
from django.contrib.auth.models import User
from polymorphic.models import PolymorphicModel
from django.utils import timezone
from crunchcore.models import Entity

"""Address book / accounts contact

Adds some extra fields to Entity"""
class Contact(Entity):
    pass
    

class Invoice(PolymorphicModel):
    pass
    

