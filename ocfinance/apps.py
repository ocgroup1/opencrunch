from django.apps import AppConfig


class OCFinanceConfig(AppConfig):
    name = 'ocfinance'
