from django.shortcuts import render, get_object_or_404
from django.views import generic, View
from django.http import HttpResponse

class StatusView(View):
    def get(self, request):
        return HttpResponse(render(request, 'ocfinance/status.html', {}))
