from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, ButtonHolder, Submit
from django.contrib.auth import forms as djangoForms

class Login(djangoForms.AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(Login, self).__init__(*args, **kwargs)
        
        self.helper = FormHelper()     
        self.helper.layout = Layout(
            Fieldset(
                "",
                Field(
                    "username", 
                    css_class="form-control-user",
                    placeholder=self.fields["username"].label,
                ),
                Field(
                    "password", 
                    css_class="form-control-user",
                    placeholder=self.fields["password"].label,
                ),
                ButtonHolder(
                    Submit("submit", "Sign In", css_class="btn-user btn-block"),
                ),
            ), 
        )
        
        self.fields["username"].label = False
        self.fields["password"].label = False

class PasswordReset(djangoForms.PasswordResetForm):
    def __init__(self, *args, **kwargs):
        super(PasswordReset, self).__init__(*args, **kwargs)
        
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                "",
                Field(
                    "email", 
                    css_class="form-control-user",
                    placeholder=self.fields["email"].label,
                ),
            ), 
        )
        
        self.fields["email"].label = False
        
class SetPassword(djangoForms.SetPasswordForm):
    def __init__(self, *args, **kwargs):
        super(SetPassword, self).__init__(*args, **kwargs)
        
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                "",
                Field(
                    "new_password1", 
                    css_class="form-control-user",
                    placeholder=self.fields["new_password1"].label,
                ),
                Field(
                    "new_password2", 
                    css_class="form-control-user",
                    placeholder=self.fields["new_password2"].label,
                ),
                ButtonHolder(
                    Submit("submit", "Change Password", css_class="btn-user btn-block"),
                ),
            ), 
        )
        
        self.fields["new_password1"].label = False
        self.fields["new_password2"].label = False
        


