# Part of OpenCrunch, see LICENSE.md and README.md
# https://gitlab.com/ocgroup1/opencrunch/
# (c) 2020 Dan Ladds AGPL v3

from django.db import models
from decimal import Decimal
from django.contrib.auth.models import User
from polymorphic.models import PolymorphicModel
from django.utils import timezone
from django_countries.fields import CountryField
from django.conf import settings as djangoSettings

import pytz
import pycountry
import phonenumbers


class Entity(PolymorphicModel):
    """A person, business, organisation, informal group, etc.
    
    Roughly follows the v-card standard with a view to CARDDAV compat
    v-card parameter names are noted where applicable"""
    class Meta:
        verbose_name_plural = "entities"
    
    """Person or org name (FN)"""
    name = models.CharField(max_length=40, null=False, blank=False)
    
    """Photo or logo (PHOTO)"""
    photo = models.ImageField(upload_to="entity-photos", null=True, blank=True, default=None)
    
    """Street address (ADR-component-street)"""
    address = models.CharField(max_length=40, null=False, blank=True, default="")
    
    """Town or village (ADR-component-locality)"""
    locality = models.CharField(max_length=40, null=False, blank=True, default="")
    
    """Region (ADR-component-region)"""
    region = models.CharField(max_length=40, null=False, blank=True, default="")
    
    """Post or Zip code (ADR-component-code"""
    postcode = models.CharField(max_length=10, null=False, blank=True, default="")
    
    """Country or nation (ADR-component-country)"""
    country = CountryField()
    
    """Timezone"""
    timezone = models.CharField(max_length=32, choices=tuple(map(lambda x, y: [x, y], pytz.all_timezones, pytz.all_timezones)), blank=False, null=False)
    
    @property
    def localtime(self):
        return timezone.localtime(timezone=self.timezone)
    
    """Other entities with which this entity has a relationship"""
    relatedEntities = models.ManyToManyField('Entity', through='EntityRelation')
    
    """Login user (if this entity has one)"""
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.SET_NULL)
    
    def __str__(self):
        return self.name

"""Language that an entity speaks

Field 'language' is an ID from/for pycountry"""
class EntityLanguage(PolymorphicModel):
    entity = models.ForeignKey(Entity, null=False, on_delete=models.CASCADE)
    language = models.CharField(max_length=7, choices=djangoSettings.LANGUAGES, null=False, blank=False)

"""Telephone number"""
class PhoneNumber(PolymorphicModel):
    entity = models.ForeignKey(Entity, null=False, on_delete=models.CASCADE)
    countryCode = models.CharField(max_length=3, blank=False, null=False)
    number = models.CharField(max_length=16, blank=False, null=False)
    sms = models.BooleanField(default=False)
    video = models.BooleanField(default=False)
    fax = models.BooleanField(default=False)
    cell = models.BooleanField(default=False)
    textphone = models.BooleanField(default=False)
    whatsapp = models.BooleanField(default=False)
    telegram = models.BooleanField(default=False)
    signal = models.BooleanField(default=False)

"""Email or instant messaging account"""
class MailMessageAddress(PolymorphicModel):
    entity = models.ForeignKey(Entity, null=False, on_delete=models.CASCADE)
    address = models.CharField(max_length=128, blank=False, null=False)
    email = models.BooleanField(default=False)
    facebook = models.BooleanField(default=False)
    twitter = models.BooleanField(default=False)
    instagram = models.BooleanField(default=False)
    linkedin = models.BooleanField(default=False)
    other = models.CharField(max_length=32, null=True, blank=True)

class EntityRelation(PolymorphicModel):
    """Relationship between two entities"""
    primaryEntity = models.ForeignKey(Entity, on_delete=models.CASCADE, related_name='related_primary_set')    
    secondaryEntity = models.ForeignKey(Entity, on_delete=models.CASCADE, related_name='related_secondary_set')
    relationship = models.CharField(max_length=255, blank=True)

class Account(PolymorphicModel):
    u"""A countable relationship between entities"""
    relation = models.ForeignKey(EntityRelation, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, blank=False)
    units = models.ForeignKey('UnitType', on_delete=models.PROTECT)
    preApprove = models.BooleanField(default=False)

class UnitType(PolymorphicModel):
    """A unit type, such as a currency"""
    name  = models.CharField(max_length=255)
    conversions = models.ManyToManyField('UnitType', through='UnitConversion')

class UnitConversion(PolymorphicModel):
    """Conversion between two unit types"""
    primaryUnit = models.ForeignKey(UnitType, on_delete=models.CASCADE, related_name='primary_conversion_set')
    secondaryUnit = models.ForeignKey(UnitType, on_delete=models.CASCADE, related_name='secondary_conversion_set')
    ratio = models.DecimalField(max_digits=30, decimal_places=2) 
    def reverse(self):
        out = UnitConversion()
        out.primaryUnit = self.secondaryUnit
        out.secondaryUnit = self.primaryUnit
        out.ratio = 1 / self.ratio
        return out

class Action(PolymorphicModel):
    """Modification to the value of an account"""
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=30, decimal_places=2)
    when = models.DateTimeField(default=timezone.now)
    approved = models.BooleanField(default=False)

class Transaction(PolymorphicModel):
    """Pair of Actions representing transfer of units between accounts"""
    primaryAction = models.ForeignKey(Action, on_delete=models.PROTECT, related_name='primary_transaction_set')
    secondaryAction = models.ForeignKey(Action, on_delete=models.PROTECT, related_name='secondary_transaction_set')


