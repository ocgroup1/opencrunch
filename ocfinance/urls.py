from django.urls import path
from . import views
from django.urls.conf import include
from django.contrib.auth import views as djangoViews

app_name = "ocfinance"

urlpatterns = [
    path("", views.Framework.as_view(), name="framework"),
    
    path("login/", views.Login.as_view(), name="login"),
    path("password/reset/", views.PasswordReset.as_view(), name="password_reset"),
    path("password/reset/sent/", views.PasswordResetSent.as_view(), name="password_reset_sent"),
    path("password/reset/confirm/<uidb64>/<token>/", views.PasswordResetConfirm.as_view(), name="password_reset_confirm"),
    path("password/reset/complete/", views.PasswordResetComplete.as_view(), name="password_reset_complete"),
    path("logout/", djangoViews.LogoutView.as_view(), name="logout"),
    path("password/change/", views.PasswordChange.as_view(), name="password_change"),
    path("password/change/done", views.PasswordChangeDone.as_view(), name="password_change_done"),
    
    path("dashboard/", views.Dashboard.as_view(), name="dashboard"),
    path("profile/", views.Profile.as_view(), name="profile"),
    
    path("contacts/", views.Contacts.as_view(), name="contacts"),
    path("contacts/all.json", views.AllContacts.as_view(), name="all_contacts"),
    path("contacts/staff.json", views.StaffData.as_view(), name="staff_data"),
    path("contacts/customers.json", views.CustomerData.as_view(), name="customer_data"),
    path("contacts/suppliers.json", views.SupplierData.as_view(), name="supplier_data"),
    path("contacts/new/", views.NewContact.as_view(), name="new_contact"),
]

# PasswordChangeView, PasswordChangeDoneView, PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
